/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  Image,
  View,
  ScrollView,
  TouchableHighlight,
  TouchableNativeFeedback 
} from 'react-native';

var style = require('./style/style');

class AwesomeProject extends Component {
  constructor(props) {
	super(props);
	this.state = { email: 'E-mail', password: 'Password' };
  }
  clickMe() {
	alert('Hi!');
  }
  render() {
	var TouchableElement = TouchableHighlight;
	if (Platform.OS === 'android') {
	 TouchableElement = TouchableNativeFeedback;
	}
	return (
		<View style={style.container}>
			<View style={style.goalHeading}>
				<Text style={style.goalHeadingText}>Self Goal</Text>
			</View>
			<TouchableElement onPress={this.clickMe.bind(this)} style={style.resultScalesButton}>
				<View style={style.buttonResultsOne}>
					<View style={style.resultScaleName}>
						<Text style={style.resultScaleNameText}>Development Confidence</Text>
						<Text style={style.resultLeaderLevel}>Beginner</Text>
					</View>
					<View style={style.resultScaleDetail}>
						<Text style={style.resultStrengthText}>BIGGEST OPPORTUNITY</Text>
						<Image source={require('./images/icon_8.png')} style={style.nextImage}>
					</Image>
					</View>
					<View style={style.resultScore}>
						<Text style={style.buttonText}>21</Text>
					</View>
				</View>
			</TouchableElement>
			<TouchableElement onPress={this.clickMe.bind(this)} style={style.resultScalesButton}>
				<View style={style.buttonResultsTwo}>
					<View style={style.resultScaleName}>
						<Text style={style.resultScaleNameText}>Optimism</Text>
						<Text style={style.resultLeaderLevel}>Capable</Text>
					</View>
					<View style={style.resultScaleDetail}>
						<Text style={style.buttonText}></Text>
						<Image source={require('./images/icon_8.png')} style={style.nextImage}>
					</Image>
					</View>
					<View style={style.resultScore}>
						<Text style={style.buttonText}>42</Text>
					</View>
				</View>
			</TouchableElement>
			
		</View>
	);
  }
}


AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);
