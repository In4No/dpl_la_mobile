<View style={style.container}>
			<View style={style.logoContainer}>
				<Image source={require('./images/Page1@3X.png')} style={style.logo}></Image>
			</View>
			<View style={style.inputContainerTop}>
				<TextInput
				style={style.textInput}
				onChangeText={(text) => this.setState({email})}
				value={this.state.email}/>
			</View>
			<View style={style.inputContainer}>
				<TextInput
				style={style.textInput}
				onChangeText={(text) => this.setState({password})}
				value={this.state.password}/>
			</View>
			<TouchableElement onPress={this.clickMe.bind(this)}>
				<View style={style.button}>
					<Text style={style.buttonText}>LOG IN</Text>
				</View>
			</TouchableElement>
			<Text style={style.forgotPassword}>Forgot Something?</Text>
			<Image source={require('./images/BG@3X.png')} style={style.footerBg}>
				<TouchableElement onPress={this.clickMe.bind(this)}>
					<View style={style.buttonTransparent}>
						<Text style={style.buttonText}>SOCIAL LOGIN</Text>
					</View>
				</TouchableElement>
			</Image>
		</View>